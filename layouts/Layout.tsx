import Head from 'next/head'
import { FC, ReactNode } from 'react'
import { Navbar } from '../components/ui';

interface Props {
  title?: string;
  children?: ReactNode;
}

const origin = (typeof window === 'undefined') ? '' : window.location.origin;

export const Layout: FC<Props> = ({ title, children }) => {

  
  return (
    <>
      <Head>
        <title>{title || 'Pokemon App'}</title>
        <meta name='author' content='Pablo W'/>
        <meta name='description' content='Pokemon ...' />
        <meta name='keywords' content='xxx, pokemon, pokedex' />
        <meta property="og:title" content={`Información sobre ${title}`} />
        <meta property="og:description" content="Get from SEO newbie to SEO pro in 8 simple steps." />
        <meta property="og:image" content={`${origin}/img/banner.png`} />
      </Head>

      <Navbar />

      <main style={{
        padding: '0 20px'
      }}>
        { children }
      </main>
    </>
  )
}