import { Grid } from "@nextui-org/react";
import { GetStaticProps, NextPage } from "next"
import { pokeApi } from "../api";
import PokeCard from "../components/cards/PokeCard";
import { PokeAPIResponse, Pokemon } from "../interfaces";
import { Layout } from "../layouts";

interface Props {
  pokemons: Pokemon[];
}

const HomePage: NextPage<Props> = ({pokemons}) => {
  return (
    <Layout title="Home Page">
     {/* listado de pokes */}
     <Grid.Container gap={2} justify='flex-start'>
      {pokemons.map((pokemon) => (
        <PokeCard pokemon={pokemon} key={pokemon.id}/>
      ))}
     </Grid.Container>

    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  const resp = await pokeApi.get<PokeAPIResponse>('/pokemon?limit=151');

  
  const pokemons: Pokemon[] = resp.data.results.map((pokemon, index) => {
    const id = index + 1;
    return {
      id,
      name: pokemon.name,
      url: pokemon.url,
      img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${id}.svg`,
    }
  })
  
  return {
    props: {
      pokemons,
    },
  }
}

export default HomePage;