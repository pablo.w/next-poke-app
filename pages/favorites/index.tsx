import { NextPage } from "next"
import { useEffect, useState } from "react";
import { NoFavs, PokeFavs } from "../../components/ui";
import { Layout } from "../../layouts";
import { localFavs } from "../../utils";

const FavoritesPage: NextPage = () => {
  const [favs, setFavs] = useState<number[]>([]);

  useEffect(() => {
    setFavs(localFavs.getFavorites());
  }, [])
  
  return (
    <Layout title="Favorites Page">
      
    {favs.length ? (
      <PokeFavs pokeList={favs} />
    ) : (
      <NoFavs />
    )} 

    </Layout>
  )
}

export default FavoritesPage;