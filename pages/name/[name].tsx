import { Button, Card, Container, Grid, Text } from '@nextui-org/react';
import { GetStaticPaths, GetStaticProps } from 'next';
import Image from 'next/image';
import React, { FC, useEffect, useState } from 'react'
import { pokeApi } from '../../api';
import { PokeAPIResponse, SinglePokeAPIResponse } from '../../interfaces'
import { Layout } from '../../layouts'
import { getPokeData, localFavs } from '../../utils';
import confetti from 'canvas-confetti';

interface Props {
  pokemon: SinglePokeAPIResponse;
}

const PokeByNamePage: FC<Props> = ({pokemon}) => {
  const [isFavorite, setIsFavorite] = useState(localFavs.checkIfFavorite(pokemon.id));

  const handleFavoriteToggle = () => {
    localFavs.toggleFavorite(pokemon.id)
    setIsFavorite(!isFavorite);

    if (isFavorite) return;
    
    confetti({
      zIndex: 99,
      particleCount: 100,
      spread: 160,
      angle: -100,
      origin: {
        x: 1,
        y: 0,
      }
    })

  }

  return (
    <Layout title={`${pokemon.name}'s Page`}>
      <Grid.Container css={{marginTop: '5px'}} gap={2}>
        <Grid xs={12} sm={4}>
          <Card hoverable css={{padding: '30px'}}>
            <Card.Body>
              <Card.Image 
                src={pokemon.sprites.other?.dream_world.front_default || '/no-image.png'}
                alt={pokemon.name}
                width='100%'
                height='200px'
              />
            </Card.Body>
          </Card>
        </Grid>

        <Grid xs={12} sm={8}>
          <Card>
            <Card.Header css={{display: 'flex', justifyContent: 'space-between'}}>
              <Text h1 transform='capitalize'>{pokemon.name}</Text>  
              <Button 
                color='gradient' 
                ghost={!isFavorite}
                onClick={handleFavoriteToggle}
              >
                {isFavorite ? 'Es Favorito!' : 'Guardar en Favoritos'}
              </Button>
            </Card.Header>

            <Card.Body>
              <Text size={30}>Sprites</Text>
              <Container direction='row' display='flex' gap={0} justify='space-around'>
                <Image 
                  src={pokemon.sprites.front_default}
                  alt={pokemon.name}
                  width={100}
                  height={100}
                />
                <Image 
                  src={pokemon.sprites.back_default}
                  alt={pokemon.name}
                  width={100}
                  height={100}
                />
                <Image 
                  src={pokemon.sprites.front_shiny}
                  alt={pokemon.name}
                  width={100}
                  height={100}
                />
                <Image 
                  src={pokemon.sprites.back_shiny}
                  alt={pokemon.name}
                  width={100}
                  height={100}
                />
              </Container>
            </Card.Body>
          </Card>
        </Grid>
      </Grid.Container>
    </Layout>
  )
}

export const getStaticPaths: GetStaticPaths = async (ctx) => {
  const { data } = await pokeApi.get<PokeAPIResponse>('/pokemon?limit=151');

  const allPokeByNamePaths = data.results.map((pokemon) => {
    return {
      params: {
        name: pokemon.name,
      },
    };
  });

  return {
    paths: allPokeByNamePaths,
    fallback: 'blocking',
  };
} 

export const getStaticProps: GetStaticProps = async ({params}) => {
  const { name } = params as { name: string };

  const pokemon = await getPokeData(name);

  if (!pokemon) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  
  return {
    props: {
      pokemon,
    },
    revalidate: 86400,
  }
}

export default PokeByNamePage