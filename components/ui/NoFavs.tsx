import { Container, Text } from '@nextui-org/react'
import Image from 'next/image'
import React from 'react'

export const NoFavs = () => {
  return (
    <Container css={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      height: 'calc(100vh - 100px)',
    }}>
      <Text h1>No hay Favoritos</Text>
      <Image 
        src={'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png'} 
        alt={'No hay favoritos'}
        width={250}
        height={250}
        style={{
          opacity: 0.3
        }}
      />
    </Container>
  )
}