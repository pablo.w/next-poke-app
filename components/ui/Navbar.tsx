import { Container, Spacer, Text, useTheme } from '@nextui-org/react'
import Image from 'next/image';
import Link from 'next/link';
import React from 'react'

export const Navbar = () => {

  const { theme } = useTheme();

  return (
    <div style={{
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'start',
      alignItems: 'center',
      width: '100%',
      padding: '0 20px',
      backgroundColor: theme?.colors.gray900.value,
    }}>
      <Link href={'/'}>
        <Container direction='row' display='flex' justify='flex-start' alignItems='center'>
          <Image 
            src={'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/132.png'} 
            alt='app icon' 
            width={70}
            height={70}
          />
          
          <Text color='white' h2>P</Text>
          <Text color='white' h3>okémon</Text>

        </Container>
      </Link>


      <Spacer css={{
        flex: 1,
      }}/>

      <Link href={'/favorites'}>
        <Text color='white'>Favoritos</Text>
      </Link>
    </div>
  )
}