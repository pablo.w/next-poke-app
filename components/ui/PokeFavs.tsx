import { Grid } from '@nextui-org/react';
import React, { FC } from 'react'
import FavPokeCard from '../cards/FavPokeCard';

interface Props {
  pokeList: number[];
}

export const PokeFavs: FC<Props> = ({pokeList}) => {
  return (
    <Grid.Container gap={2} justify='flex-start'>
    {pokeList.map((pokeId) => (
      <FavPokeCard id={pokeId} key={pokeId} />
    ))}
    </Grid.Container>
  )
}