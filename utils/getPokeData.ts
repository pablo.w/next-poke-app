import { pokeApi } from "../api";
import { SinglePokeAPIResponse } from "../interfaces";

export const getPokeData = async (nameOrId: string) => {
  
  try {
    const {data} = await pokeApi.get<SinglePokeAPIResponse>(`/pokemon/${nameOrId}`);
    
    return {
      id: data.id,
      name: data.name,
      sprites: data.sprites,
    }
  } catch (err) {
    console.error({err});
    return null;
  }
}